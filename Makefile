# $adsuck: Makefile,v 1.4 2009/03/05 19:52:18 marco Exp $

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man

PROG=adsuck
MAN=adsuck.8

SRCS= adsuck.c log.c
COPT+= -O2
DEBUG+= -ggdb3 
CFLAGS+= -Wall
CFLAGS+= -I${LOCALBASE}/include
LDFLAGS+= -L${LOCALBASE}/lib -lldns

.include <bsd.prog.mk>
